﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextScroll : MonoBehaviour
{
    public Text messageText;
    private float timer;
    public float timerPerChar;
    private string textToWrite;
    private int charIndex;

    public bool kill;
    public float killTime;
    public float killTimer;

    public bool writing;

    private void Start()
    {
        messageText.text = "";
    }

    public void AddWriter(string textToWrite, float timePerChar)
    {
        messageText.text = "";
        this.timerPerChar = timePerChar;
        this.textToWrite = textToWrite;
        charIndex = 0;
        writing = true;
    }


    public void AddWriter(string textToWrite, float timePerChar, float killTime)
    {
        AddWriter(textToWrite, timePerChar);
        kill = true;
        killTimer = killTime;
    }

    private void Update()
    {
        if (messageText != null)
        {
            timer -= Time.deltaTime;
            if (messageText.text != textToWrite)
            {
                if (timerPerChar > 0)
                {
                    if (timer <= 0f)
                    {
                        timer = timerPerChar;
                        charIndex++;
                        if (textToWrite[messageText.text.Length] == ' ')
                            messageText.text += textToWrite[messageText.text.Length];
                        messageText.text += textToWrite[messageText.text.Length];
                    }
                }
                else
                {
                    messageText.text = textToWrite;
                }
            }
            else
            {
                writing = false;
            }
        }

        if (kill && killTimer <=0)
        {
            this.messageText.text = "";
        }
        else if (!writing)
        {
            killTimer -= Time.deltaTime;
        }
    }
}
