﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndScreen : MonoBehaviour
{

    public GameObject startScreenCanvas;
    public TextScroll text;
    public GameObject clickImage;
    public AudioSource source;
    public AudioClip sound;
    public AudioClip hurray;
    private bool showButtons;
    private float waitTime;
    private bool quickShow;

    [SerializeField]
    public GameObject[] toEnable;

    public bool playEndScene;
    public bool waitingForInput = true;

    public int lineToWrite;

    private void Start()
    {
        if (playEndScene)
        {
            source.PlayOneShot(sound);
            foreach (GameObject obj in toEnable)
            {
                obj.SetActive(false);
            }
            text.AddWriter("Fin", 0.3f);
            lineToWrite = 0;
        }
    }

    private void EndGame()
    {
        Application.Quit();
    }

    // Update is called once per frame
    void Update()
    {
        if (playEndScene)
        {
            if (text.writing)
            {
                clickImage.SetActive(!text.writing);
                waitTime = 2;
            }
            else
            {
                waitTime -= Time.deltaTime;
                if (waitTime <= 0)
                {
                    if (!showButtons)
                    {
                        clickImage.SetActive(!text.writing);
                    }
                }
                if (Input.GetKeyDown(KeyCode.F) || Input.GetMouseButtonDown(0))
                {
                    lineToWrite++;
                    switch (lineToWrite)
                    {
                        case 1:
                            source.PlayOneShot(hurray);
                            text.AddWriter("Created By: \n Gianni and Sebastian Poutanen", 0.01f);
                            break;
                        case 2:
                            text.AddWriter("Thanks for playing!", 0.01f);
                            break;
                        case 3:
                            Application.Quit();
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }
}
