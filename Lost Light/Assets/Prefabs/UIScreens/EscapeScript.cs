﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EscapeScript : MonoBehaviour
{
    public float timer;
    public float exitTime;
    public Text exitText;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.Escape))
        {
            timer -= Time.deltaTime;
            exitText.text = "Hold Escape to exit " + (int)timer;
            exitText.gameObject.SetActive(true);
        }
        else
        {
            exitText.gameObject.SetActive(false);
            timer = exitTime;
        }
        if (timer <= 0)
        {
            Application.Quit(1);
        }
    }
}
