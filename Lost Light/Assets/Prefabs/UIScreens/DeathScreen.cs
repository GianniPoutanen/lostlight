﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathScreen : MonoBehaviour
{
    public GameObject startScreenCanvas;
    public TextScroll text;
    public GameObject clickImage;
    public AudioSource source;
    public AudioClip sound;
    public AudioClip crack;
    private bool showButtons;
    private float waitTime;

    [SerializeField]
    public GameObject[] toEnable;

    public bool playDeathScene;
    public bool waitingForInput = true;

    public int lineToWrite;

    private void Start()
    {
        if (playDeathScene)
        {
            source.PlayOneShot(sound);
            foreach (GameObject obj in toEnable)
            {
                obj.SetActive(false);
            }
            text.AddWriter("Something has taken you.", 0.01f);
            lineToWrite = 0;
        }
        else
        {

            source.PlayOneShot(sound);
            foreach (GameObject obj in toEnable)
            {
                obj.SetActive(false);
            }
            text.AddWriter("Start again?", 0f);

            source.PlayOneShot(sound);
        }
    }

    private void ReloadGame()
    {
        SceneManager.LoadScene("GameScene");
    }

    // Update is called once per frame
    void Update()
    {
        if (playDeathScene)
        {
            if (text.writing)
            {
                clickImage.SetActive(!text.writing);
                waitTime = 2;
            }
            else
            {
                waitTime -= Time.deltaTime;
                if (waitTime <= 0)
                {
                    if (!showButtons)
                    {
                        clickImage.SetActive(!text.writing);
                    }
                    else
                    {

                    }
                }
                if (Input.GetKeyDown(KeyCode.F) || Input.GetMouseButtonDown(0))
                {
                    lineToWrite++;
                    switch (lineToWrite)
                    {
                        case 1:
                            source.PlayOneShot(crack);
                            text.AddWriter("It's all wrong.", 0.01f);
                            break;
                        case 2:
                            text.AddWriter("Start again?", 0.01f);

                            source.PlayOneShot(sound);
                            break;
                        case 3:
                            SceneManager.LoadScene("Map");
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }
}
