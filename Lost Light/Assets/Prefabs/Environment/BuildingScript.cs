﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingScript : MonoBehaviour
{
    public Animator animator;
    public PlayerInventory player;
    public float distance;
    public RadialMenu radMenu;
    public List<GameObject> monsters;
    public GameObject endScreen;

    public float endTimer;
    public float endTime;

    public float outTimer;
    public float outTime;

    public bool outLock;
    public bool endLock;
    public bool ending;

    [SerializeField]
    public GameObject[] endMonsters;

    [SerializeField]
    public Vector2 offset;


    private void Start()
    {
        player = GameObject.Find("Player").GetComponent<PlayerInventory>();
        endLock = true;
        outLock = true;
    }

    private void Update()
    {
        if (ending)
        {
            if (Vector2.Distance(player.transform.position, this.transform.position) < distance)
            {
                Debug.Log("open menu");
                radMenu.SpawnButtons(this.GetComponent<Interactable>());
                radMenu.gameObject.SetActive(true);
            }
            else if (Vector2.Distance(player.transform.position, this.transform.position) > distance && radMenu != null)
            {
                radMenu.gameObject.SetActive(false);
            }
            else if (radMenu != null)
            {
                radMenu.transform.position = Camera.main.WorldToScreenPoint((Vector2)this.transform.position + offset);
            }
        }
        else if (outLock)
        {
            if (radMenu != null)
            {
                DestroyImmediate(radMenu.gameObject);
                radMenu = null;
            }

            if (endTimer <= 0)
            {
                foreach(GameObject monster in monsters)
                {
                    monster.SetActive(true);
                }
                outLock = false;
            }

            if (!endLock)
            {
                endTimer -= Time.deltaTime;
            }
        }
        else
        {
            endScreen.SetActive(true);
        }
    }

    public void Open()
    {
        this.animator.Play("opening");
        player.GetComponent<PlayerMovement>().canMove = false;
        endTimer = endTime;
        endLock = false;
        ending = false;
        foreach(GameObject monster in endMonsters)
        {
            monster.SetActive(true);
        }
    }

    public void UnlockTimer()
    {
        endLock = false;
    }
}
