﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRandomiser : MonoBehaviour
{

    [SerializeField]
    public GameObject[] objects;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<SpriteRenderer>().flipX = Random.value > 0.5f ? true : false;
    }

}
