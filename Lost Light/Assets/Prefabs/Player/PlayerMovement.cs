﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public float fuelMax;
    public float currentFuel;
    public float fuelConsumeRate;

    public bool inLightSource = false;
    public bool canMove = true;
    public float moveSpeed = 5f;

    public SpriteRenderer sr;
    public Animator animator;
    public Rigidbody2D rb;
    public bool craftedTorch;
    public bool holdingTorch;
    public GameObject torchLight;

    public GameObject fire;
    public float distance;
    Vector2 movement;

    public float craftRange;
    public RadialMenu radMenu;
    public Text currentNote;

    public float safetyTime;
    public float safetyTimer;

    public GameObject deathScreen;

    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            if (safetyTime <= 0)
            {
                deathScreen.SetActive(true);
                this.gameObject.SetActive(false);
            }

            movement.x = Input.GetAxisRaw("Horizontal");
            movement.y = Input.GetAxisRaw("Vertical");

            if (Vector2.Distance(this.transform.position, fire.transform.position) > SafetyDistance() && craftedTorch && currentFuel > 0)
            {
                holdingTorch = true;
                torchLight.SetActive(true);
                safetyTimer = safetyTime;
            }
            else
            {
                if (Vector2.Distance(this.transform.position, fire.transform.position) > SafetyDistance())
                    safetyTime -= Time.deltaTime;
                torchLight.SetActive(false);
                holdingTorch = false;
            }

            if (holdingTorch)
            {
                currentFuel -= fuelConsumeRate * Time.deltaTime;
                torchLight.transform.localScale = new Vector3(1, 0.7f, 1) / ( fuelMax / currentFuel);
            }
            FireInRange();
        }
    }

    public float SafetyDistance()
    {
        return this.distance * (fire.GetComponent<FireBehaviour>().percentageFill / 100);
    }

    public void FireInRange()
    {
        if ( Vector2.Distance(fire.transform.position, this.transform.position) < craftRange)
        {
            Debug.Log("open menu");
            radMenu.gameObject.SetActive(true);
            radMenu.SpawnButtons(fire.GetComponent<Interactable>());
        }
        else if (Vector2.Distance(fire.transform.position, this.transform.position) > craftRange && radMenu != null)
        {
            radMenu.gameObject.SetActive(false);
        }
        else if (radMenu.gameObject.activeSelf)
        {
            radMenu.transform.position = Camera.main.WorldToScreenPoint((Vector2)fire.transform.position);
        }
    }

    public void ResetButtons()
    {
        radMenu.SpawnButtons(fire.GetComponent<Interactable>());
    }

    public void NoteInRange()
    {
        GameObject[] notes = GameObject.FindGameObjectsWithTag("Note");
        foreach (GameObject note in notes)
        {
            if (currentNote == null && Vector2.Distance(note.transform.position, this.transform.position) < craftRange)
            {
                Debug.Log("Read Note");
                currentNote = ReadText.ins.SpawnMenu(note.transform.position);
            }
            else if (Vector2.Distance(note.transform.position, this.transform.position) > craftRange && currentNote != null)
            {
                Destroy(currentNote.gameObject);
                currentNote = null;
            }
            else if (currentNote != null)
            {
                currentNote.transform.position = Camera.main.WorldToScreenPoint((Vector2)note.transform.position);
            }
        }
    }

    void FixedUpdate()
    {
        rb.AddForce(movement.normalized * moveSpeed * Time.fixedDeltaTime);
        if (holdingTorch)
        {

            if (Mathf.Abs(movement.x) > 0.1f)
            {
                sr.flipX = (-1 == Mathf.Sign(movement.x));
                animator.Play("PlayerTorchRun");
            }
            else if (Mathf.Abs(movement.y) > 0.1f)
            {
                animator.Play("PlayerTorchRun");
            }
            else
            {
                animator.Play("PlayerTorchIdle");
            }
        }
        else if (Mathf.Abs(movement.x) > 0.1f)
        {
            sr.flipX = (-1 == Mathf.Sign(movement.x));
            animator.Play("PlayerRun");

        }
        else if (Mathf.Abs(movement.y) > 0.1f)
        {
            animator.Play("PlayerRun");

        }
        else
        {
            animator.Play("PlayerIdle");
        }
    }



}
