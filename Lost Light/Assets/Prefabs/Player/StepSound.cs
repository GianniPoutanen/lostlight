﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepSound : MonoBehaviour
{
    public AudioClip[] snowSteps;

    public void PlayRandomSnowStep()
    {
        AudioSource player = this.transform.GetComponent<AudioSource>();
        int i = (int)(Random.value * (float)(snowSteps.Length));
        if (i >= 0)
        {
            player.PlayOneShot(snowSteps[i]);
        }
    }
}
