﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour
{
    public CapsuleCollider2D collider;
    private PlayerMovement pm;
    public FireBehaviour fire;
    public GameObject closestItem;
    public List<GameObject> itemsInRange;
    public HUD HUDScript;
    public TextScroll textScroll;


    public bool firstTimeTorch;
    public bool firstTimeStone;
    public bool firstTimeAddFire;
    public int wood;
    public int stone;
    public int gems;

    // Start is called before the first frame update
    void Start()
    {
        itemsInRange = new List<GameObject>();
        pm = this.GetComponent<PlayerMovement>();
        HUDScript.wood = 0;
        HUDScript.stone = 0;
        HUDScript.gems = 0;
    }

    // Update is called once per frame
    void Update()
    {
        float closestDistance = float.MaxValue;
        foreach (GameObject obj in itemsInRange)
        {
            obj.GetComponent<ItemBehaviour>().SetHighlighted(false);
            float newDist = Vector2.Distance(obj.transform.position, this.transform.position);
            if (newDist < closestDistance)
            {
                closestDistance = newDist;
                closestItem = obj;
            }
            else
            {
                closestItem = obj;
            }
        }

        if (closestItem != null)
            closestItem.GetComponent<ItemBehaviour>().SetHighlighted(true);

        if (Input.GetKeyDown(KeyCode.F))
        {
            if (closestItem != null)
            {
                ItemPickUpHandler(closestItem.GetComponent<ItemBehaviour>().PickUp());
                itemsInRange.Remove(closestItem.gameObject);
                closestItem = null;
                //TODO add to resources
            }
        }

    }

    private void ItemPickUpHandler(string itemName)
    {
        if (itemName.Equals("Wood"))
        {
            wood++;
            HUDScript.wood = wood;
        }
        else if (itemName.Equals("Stone"))
        {
            stone++;
            HUDScript.stone = stone;
        }
        else if (itemName.Equals("Gem"))
        {
            gems++;
            HUDScript.gems = gems;
        }
        else if (itemName.Equals("Crystal"))
        {
            //TODO maybe
        }
    }


    public void TakeTorch()
    {
        if (!pm.craftedTorch)
        {
            if (firstTimeTorch)
            {
                textScroll.AddWriter("Now I can explore.", 0.01f);
                firstTimeTorch = false;
            }

            fire.percentageFill -= fire.percentageFill < 10 ? fire.percentageFill : 10;
            pm.craftedTorch = true;
            pm.currentFuel = pm.fuelMax;
        }
        else
        {
            fire.percentageFill -= (pm.fuelMax - pm.currentFuel) / fire.rateMultiplyer;
            pm.currentFuel = pm.fuelMax;
        }
        pm.radMenu.SpawnButtons(fire.GetComponent<Interactable>());
    }


    public void AddFuel()
    {
        if (this.wood > 0)
        {
            if (firstTimeAddFire)
            {
                textScroll.AddWriter("Good... I should craft a torch now.", 0.01f);
                firstTimeAddFire = false;
            }
            this.wood--;
            fire.StartGoingOut();
            HUDScript.wood = wood;
            fire.percentageFill = fire.percentageFill + 20f > 100 ? 100 : fire.percentageFill + 20;
        }
        pm.radMenu.SpawnButtons(fire.GetComponent<Interactable>());
    }

    public void UpgradeFire()
    {
        if (this.stone > 0)
        {
            if (firstTimeStone)
            {
                textScroll.AddWriter("Now the fire should last longer.", 0.01f);
                firstTimeStone = false;
            }

            this.stone--;
            HUDScript.stone = stone;
            fire.rateMultiplyer += 0.2f;
        }
        pm.radMenu.SpawnButtons(fire.GetComponent<Interactable>());
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Item" && !itemsInRange.Contains(collision.gameObject))
        {
            itemsInRange.Add(collision.gameObject);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Item" && itemsInRange.Contains(collision.gameObject))
        {
            itemsInRange.Remove(collision.gameObject);
            collision.GetComponent<ItemBehaviour>().SetHighlighted(false);
        }
    }
}
