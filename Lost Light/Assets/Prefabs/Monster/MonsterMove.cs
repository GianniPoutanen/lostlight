﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterMove : MonoBehaviour
{
    public float speed;
    public GameObject destinationObj;
    public bool killAtEnd;
    public Animator animator;
    public Vector2 destination;
    public bool runner;
    private bool running;
    public float distance;
    public GameObject player;

    public float runTime;

    private void Start()
    {
        player = GameObject.Find("Player");
        destination = destinationObj.transform.localPosition + this.transform.position;
        running = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (player.transform.position.x < this.transform.position.x)
        {
            this.GetComponent<SpriteRenderer>().flipX = true;
        }
        if (!runner)
        {
            this.transform.position = Vector2.MoveTowards(this.transform.position, destination, speed);
            if ((Vector2)this.transform.position == (Vector2)destination)
            {
                if (killAtEnd)
                {
                    Destroy(this.gameObject);
                }
                else
                {
                    animator.Play("Idle");
                }
            }
            else
            {
                animator.Play("Idle");
            }
        }
        else
        {
            if (Vector2.Distance((Vector2)this.transform.position, (Vector2)player.transform.position) < distance)
                running = true;
            if (running)
            {
                runTime -= Time.deltaTime;
                this.transform.position += ((this.transform.position - player.transform.position)).normalized * speed;
            }
        }
        if (runTime <= 0)
            Destroy(this.gameObject);
    }
}
