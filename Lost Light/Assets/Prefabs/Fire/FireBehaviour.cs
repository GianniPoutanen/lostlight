﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBehaviour : MonoBehaviour
{
    [Range(0, 100)]
    public float percentageFill;
    public GameObject lightObject;
    public ProgressBar progressBar;
    public bool goingOut;
    public float rateMultiplyer = 1;


    public float MaxReduction;
    public float MaxIncrease;
    public float RateDamping;
    public float Strength;
    public bool StopFlickering;

    private Vector3 _defaultScale;
    private GameObject _lightSource;
    private float _baseIntensity;
    private bool _flickering;


    public void Reset()
    {
        MaxReduction = 0.2f;
        MaxIncrease = 0.2f;
        RateDamping = 0.1f;
        Strength = 300;
    }

    public void Awake()
    {
        _lightSource = lightObject;
        _defaultScale = new Vector3(1, 0.7f, 1);
        if (_lightSource == null)
        {
            Debug.LogError("Flicker script must have a Light Component on the same GameObject.");
            return;
        }
        StartCoroutine(DoFlicker());
        _baseIntensity = 100;
    }


    void Update()
    {
        if (!StopFlickering)
        {
            StartCoroutine(DoFlicker());
        }
        if (goingOut)
        {
            percentageFill -= Time.deltaTime / rateMultiplyer;
            progressBar.current = (int)percentageFill;
        }
    }

    public void StartGoingOut()
    {
        progressBar.gameObject.SetActive(true);
        goingOut = true;
    }

    private IEnumerator DoFlicker()
    {
        _flickering = true;
        while (!StopFlickering)
        {
            _lightSource.transform.localScale = _defaultScale * Mathf.Lerp(percentageFill, Random.Range(percentageFill - MaxReduction, percentageFill + MaxIncrease), Strength * Time.deltaTime) / 100;
            yield return new WaitForSeconds(RateDamping);
        }
        _flickering = false;
    }
}
