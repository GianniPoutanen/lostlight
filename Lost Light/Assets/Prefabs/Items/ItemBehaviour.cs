﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBehaviour : MonoBehaviour
{
    public bool highlighted;
    public string itemName;
    public SpriteRenderer sr;
    public Sprite highlightSprite;
    public Sprite normalSprite;

    // Update is called once per frame
    void Update()
    {
        if (highlighted)
        {
            sr.sprite = highlightSprite;
        }
        else
        {
            sr.sprite = normalSprite;
        }
    }


    public void SetHighlighted(bool val)
    {
        highlighted = val;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        highlighted = false;
    }

    public string PickUp()
    {
        Destroy(this.gameObject);
        return itemName;
    }
}
