﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RadialMenu : MonoBehaviour
{
    public float distance;
    public RadialButton buttonPrefab;
    public RadialButton selected;

    [SerializeField]
    public GameObject[] buttons;


    // Start is called before the first frame update
    public void SpawnButtons(Interactable obj)
    {
        List<Interactable.Action> actions = new List<Interactable.Action>();
        foreach (Interactable.Action action in obj.options)
        {
            if (action.title == "Refuel" && GameObject.Find("Player").GetComponent<PlayerInventory>().wood > 0)
            {
                buttons[0].gameObject.SetActive(true);
            }
            else if (action.title == "Torch" && !GameObject.Find("Player").GetComponent<PlayerInventory>().firstTimeAddFire)
            {
                buttons[1].gameObject.SetActive(true);
            }
            else if (action.title == "Upgrade" && GameObject.Find("Player").GetComponent<PlayerInventory>().stone > 0)
            {
                buttons[2].gameObject.SetActive(true);
            }
            else if (action.title != "Upgrade" && action.title != "Refuel" && action.title != "Torch")
            {
                buttons[3].gameObject.SetActive(true);
            }
            else
            {

            }
        }
    }
}
