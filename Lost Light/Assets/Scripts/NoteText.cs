﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteText : MonoBehaviour
{
    public TextScroll text;
    public GameObject player;
    public float distance;
    public string textToShow;
    public string textToShowSecondLine;
    public float speed;


    private void Start()
    {
        this.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector2.Distance(this.transform.position, player.transform.position) < distance)
        {
            if (textToShowSecondLine == "")
                text.AddWriter(textToShow, 0.05f, 2f);
            else 
                text.AddWriter(textToShow + "\n" + textToShowSecondLine, speed, 2f);
            Destroy(this.gameObject);
        }
    }
}
