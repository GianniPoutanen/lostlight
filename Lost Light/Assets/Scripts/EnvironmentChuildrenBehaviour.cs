﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentChuildrenBehaviour : MonoBehaviour
{
    public Camera cam;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        foreach (Transform child in this.transform)
        {
            if (Vector2.Distance(child.transform.position, cam.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height/ 2, cam.transform.position.z))) > 30)
            {
                child.gameObject.SetActive(false);
            }
            else
            {
                child.gameObject.SetActive(true);
            }
        }
    }
}
