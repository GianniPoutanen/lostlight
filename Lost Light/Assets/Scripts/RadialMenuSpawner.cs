﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadialMenuSpawner : MonoBehaviour
{
    public static RadialMenuSpawner ins;
    public RadialMenu menuPrefab;

    void Awake(){
        ins = this;
    }

    public RadialMenu SpawnMenu(Vector2 pos, Interactable obj){
        RadialMenu newMenu = Instantiate(menuPrefab) as RadialMenu;
        newMenu.transform.SetParent(transform,false);
        newMenu.transform.position = Camera.main.WorldToScreenPoint(pos);
        newMenu.SpawnButtons(obj);
        return newMenu;
    }
}
