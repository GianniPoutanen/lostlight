﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomiseItemImage : MonoBehaviour
{
    [SerializeField]
    public DuplicateImages[] duplicates;

    [System.Serializable]
    public class DuplicateImages
    {
        public Sprite normal;
        public Sprite heighlight;
    }

    private void Start()
    {
        ItemBehaviour item = this.transform.GetComponent<ItemBehaviour>();
        int i = (int)(Random.value * (float)(duplicates.Length + 1)) - 1;
        if (i >= 0)
        {
            item.highlightSprite = duplicates[i].heighlight;
            item.normalSprite = duplicates[i].normal;
        }
        item.GetComponent<SpriteRenderer>().flipX = Random.value < 0.5f ? true : false;
    }
}
