﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DepthSimulator : MonoBehaviour
{
    public float heightOffset;
    // Update is called once per frame
    void Update()
    {
        this.transform.position = new Vector3(this.transform.position.x, transform.position.y , transform.position.y + 400 - heightOffset);
    }

}
