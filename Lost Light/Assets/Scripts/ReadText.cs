﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReadText : MonoBehaviour
{
    public static ReadText ins;
    public Text menuPrefab;
    public string displayText;
    
    void Awake(){
        ins = this;
    }

    public Text SpawnMenu(Vector2 pos){
        Text newMenu = Instantiate(menuPrefab) as Text;
        newMenu.transform.SetParent(transform,false);
        newMenu.transform.position = Camera.main.WorldToScreenPoint(pos);
        newMenu.text = displayText;
        return newMenu;
    }
}
