﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class RadialButton : MonoBehaviour, IPointerDownHandler
{
    public Image circle;
    public Image icon;
    public string title;
    public PlayerInventory playerIntventory;
    public FireBehaviour fireBehaviour;

    public void Start()
    {
        playerIntventory = GameObject.Find("Player").GetComponent<PlayerInventory>();
        fireBehaviour = GameObject.Find("Fire").GetComponent<FireBehaviour>();
    }

    private void OnMouseDown()
    {

    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("PointerDown");
        if (title == "Refuel" && playerIntventory.wood > 0)
        {
            playerIntventory.AddFuel();
            Debug.Log("F was pressed");
        }
        else if (title == "Upgrade" && playerIntventory.stone > 0)
        {
            playerIntventory.UpgradeFire();
            Debug.Log("E was Pressed");
        }
        else if (title == "Torch")
        {
            playerIntventory.TakeTorch();
            Debug.Log("E was Pressed");
        }
        else if (title == "InsertGem")
        {
            GameObject.Find("Building").GetComponent<BuildingScript>().Open();
            Debug.Log("E was Pressed");
        }
    }

}
