﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    public int wood = 0;
    public Text woodText;
    public int stone;
    public Text stoneText;
    public int gems;
    public Text metalText;

    // Update is called once per frame
    void Update()
    {
        woodText.text = "Wood: " + wood;

        stoneText.text = "Stone: " + stone;

        metalText.text = "Gems: " + gems;


        if (wood > 0)
        {
            woodText.gameObject.SetActive(true);
        }
        else if (stone > 0)
        {
            stoneText.gameObject.SetActive(true);
        }
        else if (gems > 0)
        {
            metalText.gameObject.SetActive(true);
        }
    }
}