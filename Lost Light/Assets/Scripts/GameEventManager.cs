﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEventManager : MonoBehaviour
{
    public bool gameStarted = false;
    public GameObject startScreenCanvas;
    public TextScroll text;
    public GameObject clickImage;
    public AudioSource source;
    public AudioClip sound;
    private float waitTime;

    [SerializeField]
    public GameObject[] toEnable;

    public bool playIntro;
    public bool waitingForInput = true;

    public int lineToWrite;

    private void Start()
    {
        if (playIntro)
        {
            foreach (GameObject obj in toEnable)
            {
                obj.SetActive(false);
            }
            text.AddWriter("Where did the sun go?", 0.01f);
            lineToWrite = 0;
        }
        else
        {
            StartGame();
        }
    }

    private void StartGame()
    {
        Destroy(startScreenCanvas);
        foreach (GameObject obj in toEnable)
        {
            obj.SetActive(true);
        }
        Destroy(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (playIntro)
        {
            if (text.writing)
            {
                clickImage.SetActive(!text.writing);
                waitTime = 1;
            }
            else
            {
                waitTime -= Time.deltaTime;
                if (waitTime <= 0)
                {
                    clickImage.SetActive(!text.writing);
                }
                if (Input.GetKeyDown(KeyCode.F) || Input.GetMouseButtonDown(0))
                {
                    lineToWrite++;
                    switch (lineToWrite)
                    {
                        case 1:
                            text.AddWriter("I should Light a fire.", 0.01f);
                            break;
                        case 2:
                            text.AddWriter("What's that sound!?", 0f);
                            source.PlayOneShot(sound);
                            break;
                        default:
                            StartGame();
                            break;
                    }
                }
            }
        }
    }
}
